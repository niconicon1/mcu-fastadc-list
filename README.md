# List of Microcontrollers with Fast Analog-to-Digital Converters

## Introduction

The Analog-to-Digital Converters (ADC) in most microcontrollers (MCU)
are slower than 1 Msps since their primary application is relatively slow
automatic control and measurements, not the fast data acquisition and
processing at high rates. In fact, the limited CPU speed and memory size
of many microcontrollers are incapable of keeping up with a faster
ADC, necessitates the use of a more specialized Digital Signal Processor
(DSP).

Nevertheless, for simple data acquisition tasks, MCUs with a faster ADC
can still be useful despite their limitations. For example, if the signal
processing is done on a computer, the MCU can transfer the captured data
via DMA as soon as possible with no processing, at the same time, it can
conveniently control the supporting circuitry via a few GPIO ports. Another
scenario is when only a small burst of data is needed, not sustained data.
In other words, if your problem happens to be simple enough, a MCU may be
able to solve it cheaply.

The following is a list of MCUs with fast ADCs.

## Contributing

If you have more device suggestions or comment, please open an issue or a pull request
at GitLab.

* [https://gitlab.com/niconicon1/mcu-fastadc-list](https://gitlab.com/niconicon1/mcu-fastadc-list)

## Caution

1. Even with DMA, a limited MCU throughput may still unable to support the
maximum sustained data rate of its ADC.

2. Many MCUs support interleaving multiple ADCs to multiply the sampling rate.
However, due to channel offset, gain imbalance, and timing errors, spurious
signals and distortions inevitably arise. Since removing them can be difficult,
often a single faster ADC is preferable.

3. Some MCUs lack a suitable interface to the computer, requires awkward
bridges. For example, SPI may be the only available interface. USB is usually
limited to 12 Mbps Full Speed. Some MCUs don't have *any* suitable interface
at all (again, sustained high-speed acquisition is not what they're designed
for). Thus, the list highlights High-Speed USB and Ethernet interfaces if
they're available.

4. But even then, you may need an external PHY transceiver chip to actually
use them. A MCU with builtin PHY lowers costs and complexity.

Make sure to do your own performance evaluation first before start designing
your next project using it! The legal license and disclaimer at the end applies
to this article.

## List

### NXP

* [LPC4370](https://www.nxp.com/docs/en/data-sheet/LPC4370.pdf)
    * ADC:
        1. 80 Msps, 12-bit
    * CPU:
        1. Cortex M4 @ 204 MHz
        2. Cortex M0 #1 @ 204 MHz
        3. Cortex M0 #2 @ 204 MHz
    * Interfaces: 480 Mbps USB (PHY), 100 Mbps Ethernet (MAC)
    * Packages: BGA

### Texas Instruments

* [TMS320F2823x, TMS320F2833x](https://www.ti.com/lit/ds/sprs439p/sprs439p.pdf)
    * ADC:
        1. 12.5 Msps, 12-bit
    * CPU: 100 MHz, 150 MHz
    * Packages: BGA, QFP
* [MSP430FR603X, MSP430FR604X](https://www.ti.com/lit/ds/symlink/msp430fr6035.pdf)
    * ADC:
        1. 8 Msps, 12-bit
    * CPU: MSP430 @ 16 MHz
    * Packages: QFP

### ST

* [STM32H7](https://www.st.com/en/microcontrollers-microprocessors/stm32h7-series.html)
    * ADC:
        1. 5 Msps, 12-bit x1
        2. 3.6 Msps, 16-bit x2
            * 5.5 Msps in 12-bit mode
            * 7.2 Msps (16-bit) with 2x interleaving
    * CPU: ARM Cortex M7 @ 280 MHz, 480 MHz, 550 MHz
    * Interfaces: 480 Mbps USB (ULPI), 100 Mbps Ethernet (MAC)
    * Packages: BGA, WLCSP, QFP
* [STM32F303](https://www.st.com/en/microcontrollers-microprocessors/stm32f303.html)
    * ADC:
        1. 5 Msps, 12-bit x4
            * 18 Msps with 4x interleaving
    * CPU: Cortex M4 @ 72 MHz
    * Packages: BGA, WLCSP, QFP
* [STM32F7](https://www.st.com/en/microcontrollers-microprocessors/stm32f7-series.html)
    * ADC:
        1. 2.4 Msps, 12-bit x3
            * 7.2 Msps with 3x interleaving
    * CPU: Cortex M7 @ 216 MHz
    * Interfaces: 480 Mbps USB (ULPI or PHY), 100 Mbps Ethernet (MAC)
    * Packages: BGA, WLCSP, QFP
    * Notes: On-chip USB 2.0 HS PHY on STM32F723

### Microchip

* [PIC32MK](https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/32-bit-mcus/pic32-32-bit-mcus/pic32mk)
    * ADC:
        1. 3.75 Msps, 12-bit, up to 7x
            * 15 Msps with 4x interleaving
            * 20 Msps with 6x interleaving on PIC32MKxxMCxx
            * 25.45 Msps aggregated data rate for 7 ADCs, but likely impractical.
    * CPU: MIPS32 microAptiv @ 120 MHz
    * Interfaces: 12 Mbps USB
    * Packages: QFN, TQFP
    * Notes: Complicated ADC subsystem, many modes and channels.
* [PIC32MZ EF](https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/32-bit-mcus/pic32-32-bit-mcus/pic32mz-ef) & [PIC32MZ DA](https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/32-bit-mcus/pic32-32-bit-mcus/pic32mz-da)
    * ADC:
        1. 3.125 Msps, 12-bit x5 (+ 1 shared ADC)
            * 12.5 Msps with 4x interleaving
            * 18 Msps aggregated data rate for 6 ADCs, but likely impractical.
    * CPU: MIPS32 microAptiv @ 200 MHz
    * Interfaces: 480 Mbps USB (PHY), 100 Mbps Ethernet (MAC)
    * Packages: BGA, QFN, QFP
    * Notes:
        1. Complicated ADC subsystem, many modes and channels.
        2. [ADC is defective on PIC32MZ EC](https://microchipdeveloper.com/faq:113), use PIC32MZ EF or DA instead!

### Analog Devices

* [ADSP-CM4xx](https://www.analog.com/en/products/processors-microcontrollers/microcontrollers/cm4xx-mixed-signal-control-processors.html)
    * ADC:
        1. 2.63 Msps, 16-bit x2
            * 5.26 Msps with 2x interleaving
            * 12 or 14 ENOB, depending on models
    * CPU: Cortex M4 @ 150 MHz, 240 MHz
    * Interfaces: 12 Mbps USB, 100 Mbps Ethernet
    * Packages: BGA, QFP

## License and Disclaimer

    Copyright 2021 Tom Li <tomli@tomli.me>

    Permission to use, copy, modify, and/or distribute this document for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE DOCUMENT IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS DOCUMENT INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
    OR IN CONNECTION WITH THE USE OR ACCURACY OF THIS DOCUMENT.
